clc;clear all; close all;
clc;close all;clear all;
% classes
mu1(:,1)=[5;0];mu1(:,2)=[1;1];
sigma1(:,:,1)=[0.5 0;0 0.5];sigma1(:,:,2)=[0.92 0.38;0.38 0.91];
mu2(:,1)=[0;5];mu2(:,2)=[2;2];
sigma2(:,:,1)=[1 -2;-2 10]/3;sigma2(:,:,2)=[5 1;1 3]/3;
p1=[0.4,0.6];p2=[0.1,0.9];
n=1000;
a=rand(2,n);
x=zeros(2,n);
% classifying labels
p=[0.2,0.8];
for i=1:n
if a(i)<=p(1)
    label(i)=0
    if rand(1,1)>p1(1)
        x(:,i) = mvnrnd(mu1(:,2),sigma1(:,:,2),1);
    else
        x(:,i) = mvnrnd(mu1(:,1),sigma1(:,:,1),1)';
    end
else 
    label(i)=1
    if rand(1,1)>p2(1) 
        x(:,i) = mvnrnd(mu2(:,2),sigma2(:,:,2),1)';
    else
        x(:,i) = mvnrnd(mu2(:,1),sigma2(:,:,1),1)';
    end
end
i=i+1;
end
n1=0;n2=0;
for i=1:n
    if label(i)==0
        n1=n1+1;
    else 
        n2=n2+1;
    end
end
figure(2), clf,
plot(x(1,label==0),x(2,label==0),'h'); hold on;
plot(x(1,label==1),x(2,label==1),'+'); axis equal;
legend('Class 0','Class 1');
title('Data and their true labels');
xlabel('x_1'), ylabel('x_2');
po1=p1(1)*mvnpdf(x.',mu1(:,1).',sigma1(:,:,1))+p1(2)*mvnpdf(x.',mu1(:,2).',sigma1(:,:,2));
po2=p2(1)*mvnpdf(x.',mu2(:,1).',sigma2(:,:,1))+p2(2)*mvnpdf(x.',mu2(:,2).',sigma2(:,:,2));
posti1=p(1)*po1;
posti2=p(2)*po2;
px=posti1+posti2;
for i=1:n
    if posti1(i)<posti2(i)
       decide(i)=1;
    else
        decide(i)=0;
    end
end
pw1x = posti1./px;
pw2x = posti2./px;
perrorx = min([pw1x; pw2x]).*px;

ind00 = find(decide==0 & label==0); p00 = length(ind00)/n1; % probability of true negative
ind10 = find(decide==1 & label==0); p10 = length(ind10)/n1; % probability of false positive
ind01 = find(decide==0 & label==1); p01 = length(ind01)/n2; % probability of false negative
ind11 = find(decide==1 & label==1); p11 = length(ind11)/n2; % probability of true positive
lambda = [0 1;1 0];
gamma = (lambda(2,1)-lambda(1,1))/(lambda(1,2)-lambda(2,2)) * p(1)/p(2); 
% plot correct and incorrect decisions
figure(1), % class 0 circle, class 1 +, correct green, incorrect red
plot(x(1,ind00),x(2,ind00),'og'); hold on,
plot(x(1,ind10),x(2,ind10),'or'); hold on,
plot(x(1,ind01),x(2,ind01),'+r'); hold on,
plot(x(1,ind11),x(2,ind11),'+g'); hold on,
legend(' p(x | w_1)', ' p(x | w_2)', ' Boundaries')

axis equal
discriminantScore = pw2x./pw1x;
horizontalGrid = linspace(floor(min(x(1,:))),ceil(max(x(1,:))),101);
verticalGrid = linspace(floor(min(x(2,:))),ceil(max(x(2,:))),91);
[h,v] = meshgrid(horizontalGrid,verticalGrid);
discriminantScoreGridValues = log(evalGaussian([h(:)';v(:)'],mu2(:,1),sigma2(:,:,1))+ evalGaussian([h(:)';v(:)'],mu2(:,2),sigma2(:,:,2)))-log(evalGaussian([h(:)';v(:)'],mu1(:,1),sigma1(:,:,1))+evalGaussian([h(:)';v(:)'],mu1(:,2),sigma1(:,:,2))) - log(gamma);
minDSGV = min(discriminantScoreGridValues);
maxDSGV = max(discriminantScoreGridValues);
discriminantScoreGrid = reshape(discriminantScoreGridValues,91,101);
figure(1), contour(horizontalGrid,verticalGrid,discriminantScoreGrid,[minDSGV*[0.9,0.6,0.3],0,[0.3,0.6,0.9]*maxDSGV]);
function g = evalGaussian(x,mu,Sigma)
% Evaluates the Gaussian pdf N(mu,Sigma) at each coumn of X
[n,N] = size(x);
C = ((2*pi)^n * det(Sigma))^(-1/2);
E = -0.5*sum((x-repmat(mu,1,N)).*(inv(Sigma)*(x-repmat(mu,1,N))),1);
g = C*exp(E);
end