clc;clear all;close all;
% classes
mu(:,1)=[-0.1;0];mu(:,2)=[0.1;0];
c(:,:,1)=[1 -0.9;-0.9 1];c(:,:,2)=[1 0.9;0.9 1];
p=[0.8,0.2];
n=10000;
%n = 2; % number of feature dimensions
% number of iid samples
label = rand(1,n) >= p(1);
Nc = [length(find(label==0)),length(find(label==1))]; % number of samples from each class
x = zeros(2,n); % save up space
% Draw samples from each class pdf
for l = 0:1

    x(:,label==l) = mvnrnd(mu(:,l+1),c(:,:,l+1),Nc(l+1))';
end

for i=1:n
    if label(i)==0
        n1=n1+1;
    else 
        n2=n2+1;
    end
end
figure(2), clf,
plot(x(1,label==0),x(2,label==0),'h'), hold on,
plot(x(1,label==1),x(2,label==1),'+'), axis equal,
legend('Class 0','Class 1'),
title('Data and their true labels'),
xlabel('x_1'), ylabel('x_2');
% threshold determination
discriminantScore = log(evalGaussian(x,mu(:,2),c(:,:,2)))-log(evalGaussian(x,mu(:,1),c(:,:,1)));
g=[1:n];
for i=1:n
decision = discriminantScore >= log(g(i));
ind10 = find(decision==1 & label==0); % probability of false positive
ind11 = find(decision==1 & label==1); % probability of True positive
ind00 = find(decision==0 & label==0); % probability of true negative
ind01 = find(decision==0 & label==1); % probability of false negative
    p10(i) = length(ind10)/n1;
    p11(i) = length(ind11)/n2;
    p00(i) = length(ind00)/n1;
    p01(i) = length(ind01)/n2;
    pr(i)=([p10(i),p01(i)]*Nc')/n;
end
[A,B]=min(pr(:));
figure(3);plot(p10,p11);
hold on
plot(p10(B),p11(B),'r*');
hold off
title('ROC plot');xlabel('false poisitive');ylabel('true positive');
% plot correct and incorrect decisions
figure(1), % class 0  is circle, class 1 is +, correct is blue, incorrect is red
plot(x(1,ind00),x(2,ind00),'ob'); hold on,
plot(x(1,ind10),x(2,ind10),'or'); hold on,
plot(x(1,ind01),x(2,ind01),'+r'); hold on,
plot(x(1,ind11),x(2,ind11),'+b'); hold on,
legend(' p(x | w_1)', ' p(x | w_2)', ' Boundaries')

function g = evalGaussian(x,mu,Sigma)
% Evaluates the Gaussian pdf N(mu,Sigma) at each column of X
[n,N] = size(x);
C = ((2*pi)^n * det(Sigma))^(-1/2);
E = -0.5*sum((x-repmat(mu,1,N)).*(inv(Sigma)*(x-repmat(mu,1,N))),1);
g = C*exp(E);
end

